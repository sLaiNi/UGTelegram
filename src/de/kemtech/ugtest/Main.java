package de.kemtech.ugtest;

import de.Linus122.TelegramChat.Telegram;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        Telegram telegram = de.Linus122.TelegramChat.API.getTelegramHook();
        de.Linus122.TelegramComponents.ChatMessageToTelegram chat = new de.Linus122.TelegramComponents.ChatMessageToTelegram();
        chat.text = "Server wurde gestartet!";
        chat.parse_mode = "Markdown"; // default value
        telegram.sendAll(chat);
        telegram.addListener(new TelegramListener());
        System.out.println("TelegramBot wurde geladen");
    }

    @Override
    public void onDisable() {
        System.out.println("TelegramBot wurde entaden");
    }
}
