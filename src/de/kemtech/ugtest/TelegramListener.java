package de.kemtech.ugtest;

import de.Linus122.TelegramChat.TelegramActionListener;
import de.Linus122.TelegramComponents.Chat;
import de.Linus122.TelegramComponents.ChatMessageToMc;
import de.Linus122.TelegramComponents.ChatMessageToTelegram;
import org.bukkit.Bukkit;

public class TelegramListener implements TelegramActionListener {

    @Override
    public void onSendToTelegram(ChatMessageToTelegram chatMessageToTelegram) {

    }

    @Override
    public void onSendToMinecraft(ChatMessageToMc msgObj) {
        System.out.println(msgObj.getContent());
        if(msgObj.getContent().charAt(0) == '/') {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), msgObj.getContent().substring(1));
            msgObj.setCancelled(true);
        }
    }
}